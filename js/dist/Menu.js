export class Menu {
    constructor(elements = []) {
        this.elements = elements;
        this.menuHtmlElement = document.createElement('ul');
    }
    render() {
        for (let i = 0; i < this.elements.length; i++) {
            this.elements[i].render(this.menuHtmlElement);
        }
    }
    ;
    getMenuHtmlElement() {
        return this.menuHtmlElement;
    }
}
