export class MenuItem {
    constructor(label, url, submenu) {
        this.label = label;
        this.url = url;
        this.submenu = submenu;
        this.width = 0;
        this.menuElement = document.createElement('li');
        const menuLink = document.createElement('a');
        menuLink.href = this.url;
        menuLink.innerText = this.label;
        menuLink.title = this.label;
        menuLink.addEventListener('click', e => e.preventDefault()); // just for demo purpose
        this.menuElement.appendChild(menuLink);
        if (this.submenu) {
            const menuLink = this.menuElement.children[0];
            menuLink.classList.add('submenu');
            menuLink.addEventListener('click', e => {
                e.preventDefault();
                const link = e.target;
                const ulList = link.nextSibling;
                if (link.classList.contains('active')) {
                    ulList.classList.add('fadeout');
                    setTimeout(() => {
                        link.nextSibling;
                        link.classList.remove('active');
                        ulList.classList.remove('fadeout');
                    }, 1000);
                }
                else {
                    link.classList.add('active');
                }
            });
        }
    }
    getWidth() {
        return this.width;
    }
    render(menu) {
        menu.insertAdjacentElement('beforeend', this.menuElement);
        if (this.width === 0) {
            this.width = this.menuElement.clientWidth;
        }
        if (this.submenu) {
            this.submenu.render();
            this.menuElement.appendChild(this.submenu.getMenuHtmlElement());
        }
    }
}
