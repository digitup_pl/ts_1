import {Menu} from "./Menu.js";
import {MenuItem} from "./MenuItem.js";

export class MainMenu extends Menu {

    constructor(elements: MenuItem[] = [], protected rootElement: HTMLElement) {
        super(elements);
        this.rootElement.appendChild(this.menuHtmlElement);
    }

    render() {
        super.render();
        this.setResizeEventListener();
    }

    setResizeEventListener() {
        window.addEventListener('resize', e => {
            const menuElements = <HTMLElement[]> Array.from(this.rootElement!.getElementsByTagName('ul')[0].children);
            document.getElementById('backdrop')!.classList.remove('active');
            for (let i = 0; i < menuElements.length; i++) {
                menuElements[i].remove();
            }

            let menuBarWidth = 0;
            for (let i = 0; i < this.elements.length; i++) {
                if (menuBarWidth + this.elements[i].getWidth() < window.innerWidth - 167) {
                    menuBarWidth += this.elements[i].getWidth();
                    this.elements[i].render(this.menuHtmlElement);
                }
                else {
                    const showMoreElement = this.getShowMoreElement().getElementsByTagName('ul')[0];
                    this.elements[i].render(showMoreElement);
                }

            }
        });

        window.dispatchEvent(new Event('resize'));
    }

    getShowMoreElement(): HTMLLIElement {
        const showElement = document.getElementsByClassName('showMoreMenuLi');
        if (showElement.length !== 0) {
            return <HTMLLIElement>showElement[0];
        }
        const showMore = document.createElement('li');
        showMore.classList.add('showMoreMenuLi');
        const showMoreList = document.createElement('ul');
        const showMoreLink = document.createElement('a');
        showMore.appendChild(showMoreLink);
        showMore.appendChild(showMoreList);
        showMoreLink.href = '#';
        showMoreLink.addEventListener('click', e => {
            e.preventDefault();
            const link = <HTMLAnchorElement> e.target;
            const ulList = <HTMLUListElement> link.nextSibling;
            const backdrop = <HTMLDivElement> document.getElementById('backdrop');
            let left = 0;
            if (link.classList.contains('active')) {
                if (window.innerWidth < 765) {
                    if (left >= 0) {
                        const interval2 = setInterval(() => {
                            left -= 2;
                            ulList.style.left = left + 'px';
                            if (left <= -ulList.clientWidth) {
                                clearInterval(interval2);
                                link.classList.remove('active')
                                backdrop.classList.remove('active')
                                ulList.style.left = '0';
                            }
                        }, 2);
                    }
                }
                else {
                    const ulList = <HTMLUListElement> link.nextSibling;
                    ulList.classList.add('fadeout');
                    setTimeout(() => {
                        link.nextSibling
                        link.classList.remove('active')
                        backdrop.classList.remove('active')
                        ulList.classList.remove('fadeout');
                    }, 1000);
                }
            }
            else {
                if (window.innerWidth < 765) {
                    if (left === 0) {
                        link.classList.add('active')
                        backdrop.classList.add('active')
                        left = -ulList.clientWidth;
                        ulList.style.left = left + 'px';
                        const interval = setInterval(() => {
                            left += 2;
                            ulList.style.left = left + 'px';
                            if (left >= 0) {
                                clearInterval(interval);
                            }
                        }, 2);
                    }
                }
                else {
                    link.classList.add('active')
                    backdrop.classList.add('active')
                }
            }
        });
        showMoreLink.innerText = 'Show More';
        showMoreLink.classList.add('submenu');
        showMoreLink.classList.add('showMoreMenuLink');
        this.rootElement!.getElementsByTagName('ul')[0].insertAdjacentElement('beforeend', showMore);
        return showMore;
    }
}