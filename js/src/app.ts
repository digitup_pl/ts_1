import {MenuItem} from "./MenuItem.js";
import {MainMenu} from "./MainMenu.js";
import {SubMenu} from "./SubMenu.js";

const componentsMenuItem = new MenuItem('Components', '#', new SubMenu([
    new MenuItem('Action', '/action'),
    new MenuItem('Another action', '/another-action'),
    new MenuItem('Something else', '/something-else'),
]));

const components4MenuItem = new MenuItem('Components 4', '#', new SubMenu([
    new MenuItem('Action', '/action'),
    new MenuItem('Another action', '/another-action'),
    new MenuItem('Something else', '/something-else'),
]));

const menu = new MainMenu([
    new MenuItem('Visual Language', '/language'),
    componentsMenuItem,
    new MenuItem('Components 1', '/components-1'),
    new MenuItem('Components 2', '/components-2'),
    new MenuItem('Components 3', '/components-3'),
    components4MenuItem,
    new MenuItem('Components 5', '/components-5'),
    new MenuItem('Components 6', '/components-6'),
], document.getElementById('menu')!);


menu.render();