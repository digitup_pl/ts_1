import {MenuItem} from "./MenuItem.js";

export abstract class Menu {
    protected readonly menuHtmlElement: HTMLUListElement;

    protected constructor(protected elements: MenuItem[] = []) {
        this.menuHtmlElement = <HTMLUListElement>document.createElement('ul')
    }

    render() {
        for (let i = 0; i < this.elements.length; i++) {
            this.elements[i].render(this.menuHtmlElement);
        }
    };

    getMenuHtmlElement() {
        return this.menuHtmlElement;
    }

}