import {Menu} from "./Menu.js";
import {MenuItem} from "./MenuItem";

export class SubMenu extends Menu {

    constructor(elements: MenuItem[] = []) {
        super(elements);
    }

}