import {SubMenu} from "./SubMenu.js";

export class MenuItem {
    private readonly menuElement: HTMLLIElement;
    private width: number;

    constructor(private label: string, private url: string, private submenu?: SubMenu) {
        this.width = 0;
        this.menuElement = <HTMLLIElement>document.createElement('li');
        const menuLink = <HTMLAnchorElement>document.createElement('a');
        menuLink.href = this.url;
        menuLink.innerText = this.label;
        menuLink.title = this.label;
        menuLink.addEventListener('click', e => e.preventDefault()); // just for demo purpose
        this.menuElement.appendChild(menuLink);

        if (this.submenu) {
            const menuLink = <HTMLAnchorElement> this.menuElement.children[0];
            menuLink.classList.add('submenu');
            menuLink.addEventListener('click', e => {
                e.preventDefault();
                const link = <HTMLAnchorElement> e.target;
                const ulList = <HTMLUListElement> link.nextSibling;
                if (link.classList.contains('active')) {
                    ulList.classList.add('fadeout');
                    setTimeout(() => {
                        link.nextSibling
                        link.classList.remove('active')
                        ulList.classList.remove('fadeout');
                    }, 1000);
                }
                else {
                    link.classList.add('active')
                }
            });
        }
    }

    getWidth() {
        return this.width;
    }

    render(menu: HTMLUListElement) {
        menu.insertAdjacentElement('beforeend', this.menuElement)
        if (this.width === 0) {
            this.width = this.menuElement.clientWidth;
        }
        if (this.submenu) {
            this.submenu.render();
            this.menuElement.appendChild(this.submenu.getMenuHtmlElement());
        }
    }

}